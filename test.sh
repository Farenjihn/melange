#!/bin/bash

die() {
    echo "$1"
    exit 1
}

usage() {
    die "./test.sh <distro>"
}

[[ -x $(which mkosi) ]] || die "mkosi is not installed"
[[ -x $(which debootstrap) ]] || die "debootstrap is not installed"

if [ "$#" -ne 1 ]; then
    usage
fi

distro="$1"

mkdir -p mkosi.cache
mkdir -p mkosi.output

[[ -f mkosi.default ]] && rm mkosi.default

case "${distro}" in
"debian10")
    ln -s distro/buster mkosi.default
    ;;
"debian11")
    ln -s distro/bullseye mkosi.default
    ;;
*)
    die "unknown distribution: ${distro}"
    ;;
esac

sudo mkosi -i -f
sudo mkosi qemu &

mkosi ssh /usr/bin/melange-test.sh
