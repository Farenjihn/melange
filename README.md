# Melange

## What is this ?

This project is a simple showcase of a kernel module using rootkit techniques to provide a few features. Currently it can:

- Patch the syscall table to hide files beginning with a certain prefix
- Escalate privileges for a process that writes the proper password to a character device, with its hash being computed in kernel space and compared to a known value

In the future, this project could integrate more features such as:

- Process hiding
- An `ioctl` interface to control the module from userspace
- Patching the `kill` syscall to disallow sending SIGKILL to some processes
- etc...

A companion userspace process could then use those features to serve as a permanent backdoor that would set up persistency or obey commands from a C&C channel. The use of eBPF filters or the like could be interesting to help hiding UDP traffic.

## Where's the code ?

You can find the kernel module code under `mkosi.extra/usr/src/melange`.

## Can I test it ?

Sure, to do that you will need to install [`mkosi`](https://github.com/systemd/mkosi), `qemu` and `debootstrap`. You can then run `./test.sh debian10` to automatically create a minimal Debian 10 VM image and run a test script that outputs its results after building and inserting the module. This very simple test script supports Debian 10 and 11 for now.
