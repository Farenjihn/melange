#include "crypto.h"

#include <crypto/hash.h>

int melange_hash(char *alg_name, char *data, unsigned int len, char *digest)
{
	struct crypto_shash *tfm;
	struct shash_desc *desc;
	int desc_size;
	int ret;

	tfm = crypto_alloc_shash(alg_name, 0, 0);
	if (IS_ERR(tfm)) {
		return PTR_ERR(tfm);
	}

	desc_size = crypto_shash_descsize(tfm) + sizeof(desc);
	desc = kzalloc(desc_size, GFP_KERNEL);
	if (!desc) {
		ret = -ENOMEM;
		goto cleanup_tfm;
	}

	desc->tfm = tfm;
	ret = crypto_shash_digest(desc, data, len, digest);
	kfree(desc);

cleanup_tfm:
	crypto_free_shash(tfm);
	return ret;
}
