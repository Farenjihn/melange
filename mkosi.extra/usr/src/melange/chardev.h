#ifndef MELANGE_CHARDEV
#define MELANGE_CHARDEV

int melange_init_chardev(void);
void melange_clean_chardev(void);

#endif
