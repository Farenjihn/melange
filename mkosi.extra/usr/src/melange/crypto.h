#ifndef MELANGE_CRYPTO
#define MELANGE_CRYPTO

int melange_hash(char *alg_name, char *data, unsigned int len, char *digest);

#endif
