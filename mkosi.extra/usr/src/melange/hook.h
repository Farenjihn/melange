#ifndef MELANGE_HOOK
#define MELANGE_HOOK

#include <linux/linkage.h>

void melange_find_syscall_table(void);

void melange_hook_syscall_table(void);
void melange_unhook_syscall_table(void);

#endif
